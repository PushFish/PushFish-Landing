# PushFish-landing

The homepage of PushFish.
[https://push.fish/](https://push.fish/)

Based on [Jekyll Material Theme](https://github.com/alexcarpenter/material-jekyll-theme), and using [Devicon](https://github.com/konpa/devicon)

![Screenshot](/img/screenshot.png "Screenshot")


## Build Instructions
1. Install [Ruby](https://www.ruby-lang.org/en/downloads/) and [Jekyll](https://jekyllrb.com/docs/installation/) environments
1. Install `bundle` Ruby gem:
  `gem install bundle`
1. Clone the repository, and choose a branch. Development branch is `master`, and the live version is `production`:
    ```
    git clone https://gitlab.com/pushfish/pushfish-landing.git
    cd $(pwd)/pushfish-landing
    git checkout $branch
    ```
1. Install gems:
  `bundle install`
1. Run Jekyll webserver:
  `bundle exec jekyll serve`
1. Open http://localhost:4000 in your web browser

## Contributing

PushFish is a community project and is always welcoming of contributions and suggestions. By contributing, you are agreeing that you have read and agree to [contributing.md](/contributing/), and [legal.md](/legal/).

Questions? Ask us on [Slack](https://join.slack.com/t/pushjetfork/shared_invite/enQtNDMzNDk2MTYwOTQ5LWQ4ODZiMjQ3ZjQwNjQ4Y2NjMGMxNjVmYzNhNWZmNDgzMDg5NmY1YzA5NTIzMTk2NmRlMzI3YWE1OTE0ZDZkOTY) or open an issue on one of our [GitLab repos](https://gitlab.com/PushFish/)!

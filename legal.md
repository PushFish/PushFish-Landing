---
layout: page
permalink: /legal/
title: PushFish - Legal
---

# PushFish - Contributor License Agreement

<br/>

Please select the appropriate Contributor License Agreement (CLA) based on if you are an [individual](/legal/individual-cla/) or [corporate](/legal/corporate-cla/) contributor.
<br/>
<br/>
<br/>
<br/>
<br/>